#!/bin/python
from decimal import *
#jan feb mar
#apr may jun
#jul aug sep
#oct nov dec

contrattoKW = 3
messe = 2

f1 = int(input("F1: ") or 51)
f23 = int(input("F23: ") or 114)
print("\n---- ANALISI ----")

#Spesa per la materia energia
meQuotaFissa = (4.2617 * messe)
#01/10/2020 - 31/12/2020
# meF1 = 0.074810 * f1
# meF23 = 0.062340 * f23
#prices calculated with
#https://copernicoitalia.it/controllo-bolletta-enel/#f1p0|f2p0|f3p1 maybe for previous trimester
meF1 = (0.04516 * f1)
meF23 = (0.04200 * f23)
meTotale = meQuotaFissa + meF1 + meF23
print("Materia Energia:", meTotale, "€")
print("\t Quota Fissa:", meQuotaFissa, "€")
print("\t Quota Energia F1:", meF1, "€")
print("\t Quota Energia F23:", meF23, "€")

#Spesa per transporto
tQuotaFissa = 1.7 * messe
tQuotaPotenza = 1.74 * messe * contrattoKW
tQuotaVariabile = 0.008330 * (f1 + f23)
tTotale = tQuotaFissa + tQuotaPotenza + tQuotaVariabile
print("\nTransporto: ", tTotale , "€")
print("\t Quota Fissa:", tQuotaFissa, "€")
print("\t Quota Potenza:", tQuotaPotenza, "€")
print("\t Quota Variabile:", tQuotaVariabile, "€")
#Spesa per Oneri di Sistema
#01/10/2020 - 31/12/2020
# oQuotaFissa = 9.571900 * messe
#01/07/2020 - 31/09/2020
oQuotaFissa = 9.341000 * messe
oQuotaVariabile = 0.041817 * (f1 + f23)
oTotale = oQuotaFissa + oQuotaVariabile
print("\nOneri di Sistema: ", oTotale , "€")
print("\t Quota Fissa:", oQuotaFissa, "€")
print("\t Quota Variabile:", oQuotaVariabile, "€")

totale = meTotale + tTotale + oTotale 
accisa = (f1 + f23) * 0.0227
iva = (totale + accisa) * 0.1
print("\nTotale Spesa energia/transporto/oneri: ", totale, "€")
print("Totale Imposte e Iva: ", accisa + iva, "€")
print("\tAccisa su kWh:", f1 + f23, "kWh * 0.022700 €/KWh:", accisa, "€")
print("\tIVA 10%:", iva, "€")
print("---- RISUTALTI ----")
print("Grand Totale: ", totale + accisa + iva, "€")
if((f1 + f23) != 0):
    print("Grand Totale/kWh: ", (totale + accisa + iva)/(f1 + f23), "€")


