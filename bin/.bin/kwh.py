#!/bin/python

W = int(input("Device wattage: ") or 1)
duration = int(input("Active days: ") or 1)
hours = float(input("Active hours per day: ") or 1)
kWh = W * hours * duration / 1000
print("Total kWh: ", kWh)

#average cost of kwh 
F1 = 0.074810
F23 = 0.062340
# F1 = 0.04516
# F23 = 0.04200
F123 = (F1 + F23)/2

mvar = F123 * kWh
tvar = 0.008330 * kWh
ovar = 0.041817 * kWh
acci = 0.0227 * kWh
sum = (mvar + tvar + ovar + acci) 
sum *= 1.1

print("Cost/kWh:", sum/kWh, "€")
print("Cost:", sum, "€")
