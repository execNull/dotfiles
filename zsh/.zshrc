if [[ "$TERM" == "dumb" ]]
then
    unsetopt zle
    unsetopt prompt_cr
    unsetopt prompt_subst
    # unfunction precmd
    # unfunction preexec
    PS1="$ "
    export PS1="$ "
    return
fi

HISTFILE=~/.histfile
HISTSIZE=10000
SAVEHIST=10000

setopt appendhistory autocd beep extendedglob nomatch notify correctall
unsetopt BAD_PATTERN
bindkey -e

#share history in between sessions
setopt inc_append_history
setopt hist_ignore_dups
setopt hist_ignore_space

zstyle :compinstall filename '/home/zofos/.zshrc'

autoload -Uz compinit
compinit

zprompt() {
    #Accept three colors as input, if some are missing set default values
    local zprompt_user_user=${1:-'yellow'}
    local zprompt_host_local=${2:-'blue'}
    local zprompt_user_root=${3:-'red'}
    local zprompt_host_remote=${4:-'cyan'}
    local zprompt_path=${5:-'green'}

    if [ "$UID" = '0' ]
    then
        local user_prompt="%B%F{$zprompt_user_root}%n%f"
    else
        local user_prompt="%B%F{$zprompt_user_user}%n%f"
    fi
    local at_prompt="%B%F{$zprompt_path}@%k"

    if [ -n "${SSH_TTY}" ]
    then
        local host_prompt="%B%F{$zprompt_host_remote}%U%m%f%u"
    else
        local host_prompt="%B%F{$zprompt_host_local}%m%f"
    fi

    local path_prompt="%B%F{$zprompt_path}%~%k"
    local post_prompt="%b%f%k"
    typeset -g PS1="$user_prompt$at_prompt$host_prompt $path_prompt %# $post_prompt"
    typeset -g PS2="$user_prompt$at_prompt$host_prompt $path_prompt %_> $post_prompt"
    typeset -g PS3="$user_prompt$at_prompt$host_prompt $path_prompt ?# $post_prompt"
}

zprompt

# colored completion - use my LS_COLORS
eval $(dircolors -b $HOME/.dircolors)
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*:descriptions' format '%U%B%d%b%u'
zstyle ':completion:*:warnings' format '%BSorry, no matches for: %d%b'
#fix capitalization
zstyle ':completion:*' matcher-list '' 'm:{a-zA-Z}={A-Za-z}'
zstyle ':completion:*' menu select

#autosuggestions
source ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh
ZSH_AUTOSUGGEST_STRATEGY='completion'
ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=white'
bindkey '^j' autosuggest-execute
bindkey '^o' autosuggest-accept

#syntax highlighting in the command line
source ~/.zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

#history substring search
source ~/.zsh/zsh-history-substring-search/zsh-history-substring-search.zsh
zmodload zsh/terminfo
bindkey "$terminfo[kcuu1]" history-substring-search-up
bindkey "$terminfo[kcud1]" history-substring-search-down
bindkey -M emacs '^P' history-substring-search-up
bindkey -M emacs '^N' history-substring-search-down

precmd () {print -Pn "\e]0;%n@%m: %~\a"}

[ -n "$EAT_SHELL_INTEGRATION_DIR" ] && \
  source "$EAT_SHELL_INTEGRATION_DIR/zsh"

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/usr/bin/conda' 'shell.zsh' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/usr/etc/profile.d/conda.sh" ]; then
        . "/usr/etc/profile.d/conda.sh"
    else
        export PATH="/usr/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<

