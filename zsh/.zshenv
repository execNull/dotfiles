export EDITOR="emacsclient -t"
export QT_QPA_PLATFORMTHEME=qt6ct
export QT_QPA_PLATFORM=wayland

export ELECTRON_OZONE_PLATFORM_HINT="auto"
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CONFIG_CACHE="$HOME/.cache"
export XDG_DATA_HOME="$HOME/.local/share"
export RANGER_LOAD_DEFAULT_RC="FALSE"
export PATH="$PATH:$HOME/.local/bin:$HOME/.bin"

export LIBVA_DRIVER_NAME="iHD"

alias ls="ls --color=auto -N"
alias grep="grep --color=auto"
alias ip="ip --color=auto"
alias vi=vim
alias more=less
alias uctl='systemctl --user'
alias ssh='TERM=xterm ssh'
alias it='trans :it'
alias el='trans :el'
alias ducks='du -cksh * | sort -hr '
alias grep='grep --color'
alias toggle_laptop_display='swaymsg "output eDP-1 toggle"'
# alias sway='proptest -M i915 -D /dev/dri/card1 317 connector 312 1; proptest -M i915 -D /dev/dri/card1 317 connector 322 12; sway'
#alias pixel="exec grim -g \"$(slurp -p)\" -t ppm - | convert - -format '%[pixel:p{0,0}]' txt:- | tail -n 1 | cut -d ' ' -f 4"
