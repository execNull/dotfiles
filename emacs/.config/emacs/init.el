(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
;; Comment/uncomment this line to enable MELPA Stable if desired.  See `package-archive-priorities`
;; and `package-pinned-packages`. Most users will not need or want to do this.
;;(add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/") t)
(package-initialize)

(use-package delight :ensure t)

(use-package magit
  :ensure t
  :bind ("C-c g" . 'magit-status))

(use-package modus-themes
  :ensure t
  :config
  (setq modus-themes-italic-constructs t
	modus-themes-bold-constructs nil)
  (setq modus-themes-common-palette-overrides
        `(
	  (bg-mode-line-active bg-lavender)
          (bg-mode-line-inactive bg-dim)
          (border-mode-line-active bg-mode-line-active)
          (border-mode-line-inactive bg-mode-line-inactive)
          (bg-hl-line bg-sage)
          (bg-tab-bar bg-dim)
          (bg-tab-current bg-lavender)
          (bg-tab-other bg-main)
          ;; (bg-line-number-active bg-main)
          ;; (bg-line-number-inactive bg-main)
          ;; (fg-line-number-inactive bg-active)
          ;; (cursor red)
          (fringe bg-main)
          ;; ,@modus-themes-preset-overrides-faint
          ))
  (setq modus-themes-to-toggle '(modus-vivendi modus-operandi))
  :bind ("<f5>" . modus-themes-toggle))

(use-package tramp
  :config
  (setq tramp-default-method "ssh"))

(use-package eglot
  :config
  (add-to-list 'eglot-server-programs
	       '((qml-mode) . ("qmlls6"))
	       ))

(use-package qml-mode
  :bind (("C-c C-r" . (lambda()
		       (interactive)
		       (async-shell-command (concat "qml6 " (buffer-file-name))))))
  ("C-c C-f" . eglot-format-buffer)
  )

;; (use-package PDFView
  ;; :bind (("j" . pdf-view-next-line-or-next-page)
         ;; ("k" . pdf-view-previous-line-or-previous-page)))


(use-package avy
  :ensure t
  :config
  (setq avy-timeout-seconds 1)
  :bind(("C-; w" . avy-goto-word-1)
        ("C-; p" . avy-goto-line-above)
        ("C-; n" . avy-goto-line-below)
        ("C-; c" . avy-goto-char)
        ("C-; t" . avy-goto-char-timer)
        ))

(use-package ace-window
  :ensure t
  :config
  (setq aw-dispatch-always t)
  (setq ace-window-display-mode t)
  :bind("C-; C-;" . 'ace-window)
  )

(use-package windmove
  :bind(("C-; C-h" . windmove-left)
	("C-; C-l" . windmove-right)
	("C-; C-k" . windmove-up)
	("C-; C-j" . windmove-down)))

(use-package marginalia
  :ensure t
  :bind (("M-A" . marginalia-cycle)
         :map minibuffer-local-map
         ("M-A" . marginalia-cycle))
  :init
  (marginalia-mode)
  :config
  ;; (setq marginalia-field-width 50)
  )

(use-package which-key
  :ensure t
  :delight
  :config
  (which-key-mode))

(use-package eshell
  :requires emacs
  :hook (eshell-mode . (lambda ()
			 (setq-local global-hl-line-mode nil))))

(use-package tab-bar
  :config
  ;; Tab-bar-configuration
  (setq tab-bar-close-button-show nil
        tab-bar-new-button-show nil
        tab-bar-tab-hints t
        tab-bar-select-tab-modifiers '(meta))
  (tab-bar-mode 1))

(use-package emacs
  :init
  (setq completion-cycle-threshold 3)
  (setq tab-always-indent 'complete)
  :config
  ;; Appearance
  (menu-bar-mode -1)
  (tool-bar-mode -1)
  (scroll-bar-mode -1)
  (load-theme 'modus-vivendi :no-confirm)
  (setq use-dialog-box nil
	inhibit-splash-screen t)

  (setq redisplay-dont-pause t
  scroll-margin 1
  scroll-step 1
  scroll-conservatively 10000
  scroll-preserve-screen-position 1)
  (setq jit-lock-defer-time 0)
  (setq fast-but-imprecise-scrolling t)

  ;; Retrieve the extra settings from the custom file
  (setq custom-file (expand-file-name "custom.el" user-emacs-directory))
  (load custom-file)

  ;; Useful stuff
  (show-paren-mode 1)
  (electric-pair-mode 1)
  (recentf-mode 1)
  (column-number-mode 1)
  (blink-cursor-mode 0)
  (fido-vertical-mode 1)
  (pixel-scroll-precision-mode 1)
  (global-hl-line-mode 1)
  (save-place-mode 1)
  (global-auto-revert-mode 1)
  (setq visible-bell 1)
  (setq switch-to-buffer-obey-display-actions t)
  ;; Backup and lock files
  (setq create-lockfiles nil
        backup-by-copying t            ;Always copy to create backup files
        backup-directory-alist `((".*" . "~/.cache/emacs/"))
				 ;; (file-name-concat user-emacs-directory "backup")))
        delete-old-versions t           ;Delete excess backups silently
        kept-new-versions 2
        kept-old-versions 2
        version-control t               ;Make numeric backup versions unconditionally
        create-lockfiles nil            ;Do we really care about editing collisioins? We should, but screw it.
        vc-follow-symlinks t            ;Always visit the real file, not its link
	auto-save-file-name-transforms `((".*" "~/.cache/emacs/"))
	)
  (add-to-list 'major-mode-remap-alist '(python-mode . python-ts-mode))
  :delight
  (eldoc-mode)
  (auto-revert-mode)
  (hi-lock-mode)
  (abbrev-mode)

  :bind (("C-z" . 'execute-extended-command)
	 ("C-c e" . 'eval-buffer)
	 ("C-c O" . 'open-init-file)
	 ("C-x C-2" . 'mp-split-below)
         ("C-x C-3" . 'mp-split-right)
	 ("C-; o" . 'occur)
	 ("C-; C-n" . 'next-error)
	 ("C-; C-p" . 'previous-error)
	 )
  :hook
  (before-save . delete-trailing-whitespace)
  )

;; Functions
(defun open-init-file ()
  "Edit the `user-init-file` in this window."
  (interactive)
  (find-file user-init-file))

(defun mp-split-below (arg)
  "Split window below from the parent or from root with ARG."
  (interactive "P")
  (split-window (if arg (frame-root-window)
                  (window-parent (selected-window)))
                nil 'below nil))

(defun mp-split-right (arg)
  "Split window right from the parent or from root with ARG."
  (interactive "P")
  (split-window (if arg (frame-root-window)
                  (window-parent (selected-window)))
                nil 'right nil))

(provide 'init)
(put 'dired-find-alternate-file 'disabled nil)
