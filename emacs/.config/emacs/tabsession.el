;; -*- mode: emacs-lisp; lexical-binding:t; coding: utf-8-emacs; -*-
;; -------------------------------------------------------------------------
;; Tabspaces Session File for Emacs
;; -------------------------------------------------------------------------
;; Created Wed Mar  6 21:43:28 2024

;; Tabs and buffers:
(setq tabspaces--session-list '((nil . "*scratch*")))