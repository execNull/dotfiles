;;Add the melpa repository to the archive list. Necessary to be able to load use-package
(require 'package)
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))

;;If use-package is not installed, find it and install it
(package-initialize)
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(eval-when-compile
  (require 'use-package))

(use-package emacs
  :init
  (menu-bar-mode -1)                    ;Hide menu bar
  (tool-bar-mode -1)                    ;Hide tool bar
  (scroll-bar-mode -1)                  ;Hide scroll bar
  (show-paren-mode 1)                   ;Show matching parentheses
  (global-hl-line-mode 1)               ;Highlight current line in all buffers
  (fido-vertical-mode 1)                ;Enable fido
  (electric-pair-mode 1)                ;Add stuff in pairs
  (recentf-mode 1)                      ;Show recent files
  (pixel-scroll-precision-mode 1)
  (tab-bar-mode 1)
  (blink-cursor-mode 0)
  (delete-selection-mode 1)
  (setq use-dialog-box nil              ;Don’t use dialog boxes for questions
        show-paren-delay 0              ;Don't wait before showing matching parenthesis
        inhibit-splash-screen t         ;Hide the splash screen
        recentf-max-menu-items 25       ;Increase number of items in recentf list
        column-number-mode t            ;Display the column number in the bar
        backup-by-copying t             ;Always copy to create backup files
        backup-directory-alist `((".*" . "~/.cache/emacs/"))
        delete-old-versions t           ;Delete excess backups silently
        kept-new-versions 2
        kept-old-versions 2
        version-control t               ;Make numeric backup versions unconditionally
        create-lockfiles nil            ;Do we really care about editing collisioins? We should, but screw it.
        vc-follow-symlinks t            ;Always visit the real file, not its link
        custom-file (expand-file-name "custom.el" user-emacs-directory)
        auto-save-file-name-transforms `((".*" ,temporary-file-directory t))
        scroll-conservatively 101       ;Stop centering the cursor on screen
        scroll-margin 5                 ;Lines below or above the cursor line
        visible-bell 1                  ;Flash frame to represent a bell
        switch-to-buffer-obey-display-actions t) ;Treat manual buffer switching
                                                 ;the same as programmatic switching
  (setq-default show-trailing-whitespace nil	 ;Hide trailing whitespace
                indicate-empty-lines nil ;Hide empty lines
                indent-tabs-mode nil)
  ;;Tab-bar-mode configuration
  (setq tab-bar-close-button-show nil
        tab-bar-new-button-show nil
        tab-bar-tab-hints t
        tab-bar-select-tab-modifiers '(meta))
  ;;Tramp configuration
  (setq tramp-default-method "ssh")
  ;;Tree sitter grammar
  (setq treesit-language-source-alist
   '((bash "https://github.com/tree-sitter/tree-sitter-bash")
     (cmake "https://github.com/uyha/tree-sitter-cmake")
     (c "https://github.com/tree-sitter/tree-sitter-c")
     (css "https://github.com/tree-sitter/tree-sitter-css")
     (c++ "https://github.com/tree-sitter/tree-sitter-cpp")
     (elisp "https://github.com/Wilfred/tree-sitter-elisp")
     ;; (go "https://github.com/tree-sitter/tree-sitter-go")
     ;; (html "https://github.com/tree-sitter/tree-sitter-html")
     ;; (javascript "https://github.com/tree-sitter/tree-sitter-javascript" "master" "src")
     (json "https://github.com/tree-sitter/tree-sitter-json")
     (make "https://github.com/alemuller/tree-sitter-make")
     (markdown "https://github.com/ikatyang/tree-sitter-markdown")
     (python "https://github.com/tree-sitter/tree-sitter-python")
     (toml "https://github.com/tree-sitter/tree-sitter-toml")
     ;; (tsx "https://github.com/tree-sitter/tree-sitter-typescript" "master" "tsx/src")
     ;; (typescript "https://github.com/tree-sitter/tree-sitter-typescript" "master" "typescript/src")
     (yaml "https://github.com/ikatyang/tree-sitter-yaml")))
  (setq major-mode-remap-alist
        '((yaml-mode . yaml-ts-mode)
          ;; (bash-mode . bash-ts-mode)
          ;; (js2-mode . js-ts-mode)
          ;; (typescript-mode . typescript-ts-mode)
          ;; (json-mode . json-ts-mode)
          ;; (css-mode . css-ts-mode)
          (c++-mode . c++-ts-mode)
          (c-mode . c-ts-mode)
          (c-or-c++-mode . c-or-c++-ts-mode)
          (python-mode . python-ts-mode)))
  :bind (("C-z"     . 'undo)
         ("C-c e"   . 'eval-buffer)
         ("<f7>"    . 'compile)
         ("<65300>" . 'scroll-lock-mode)
         ("C-c s"   . 'raise-shell)
         ("C-c O"   . 'open-init-file)
         ("C-x C-r" . 'recentf-open-files)
         ("C-x C-2" . 'mp-split-below)
         ("C-x C-3" . 'mp-split-right))
  :config
  (load custom-file)
  :delight
  (eldoc-mode)
  (auto-revert-mode)
  (abbrev-mode)
  (hi-lock-mode)
  :hook
  (before-save . delete-trailing-whitespace)
  )

(use-package display-line-numbers
  :ensure nil
  :config
  (setq-default display-line-numbers-type t
                display-line-numbers-width-start t)
  :hook ((c++-mode        . display-line-numbers-mode)
         (c++-ts-mode     . display-line-numbers-mode)
         (python-mode     . display-line-numbers-mode)
         (cmake-mode      . display-line-numbers-mode)
         (emacs-lisp-mode . display-line-numbers-mode)))

(use-package delight
  :ensure t)

(use-package modus-themes
  :ensure t
  :demand t
  :config
  ;; (setq modus-themes-italic-constructs nil
        ;; modus-themes-bold-constructs nil
        ;; modus-themes-variable-pitch-ui nil
        ;; modus-themes-custom-auto-reload t
        ;; modus-themes-disable-other-themes t)

  (setq modus-themes-common-palette-overrides
        `(
          (bg-mode-line-active bg-green-subtle)
          (bg-mode-line-inactive bg-dim)
          (border-mode-line-active bg-mode-line-active)
          (border-mode-line-inactive bg-mode-line-inactive)
          (bg-hl-line bg-red-nuanced)
          (bg-tab-bar bg-dim)
          (bg-tab-current bg-green-subtle)
          (bg-tab-other bg-inactive)
          (bg-line-number-active bg-main)
          (bg-line-number-inactive bg-main)
          (fg-line-number-inactive bg-active)
          (border bg-dim)
          (cursor red)
          (fringe bg-main)
          ;; ,@modus-themes-preset-overrides-faint
          ))
  ;; Load the theme of your choice.
  (load-theme 'modus-vivendi :no-confirm)
  :bind ("<f5>" . modus-themes-toggle))

;; (use-package faces
  ;; :defer t
  ;; :custom-face
  ;; (variable-pitch ((t (:family "Monospace" :height 100))))
  ;; (fixed-pitch ((t (:family "Monospace" :height 100))))
  ;; (default ((t (:family "Monospace" :height 110)))))


(use-package eshell
  :defer t
  :hook
  (eshell-mode . (lambda ()
                   (setq-local scroll-margin 0)
                   (hl-line-mode nil))))

(use-package avy
  :ensure t
  :config
  (setq avy-timeout-seconds 10)
  :bind(("C-; w" . avy-goto-word-1)
        ("C-; p" . avy-goto-line-above)
        ("C-; n" . avy-goto-line-below)
        ("C-; c" . avy-goto-char)
        ("C-; t" . avy-goto-char-timer)
        ))

(use-package ace-window
  :ensure t
  :config
  (setq aw-dispatch-always t)
  :bind("C-; C-;" . 'ace-window)
  )

(use-package windmove
  :bind("C-; C-h" . windmove-left)
  :bind("C-; C-l" . windmove-right)
  :bind("C-; C-k" . windmove-up)
  :bind("C-; C-j" . windmove-down))

(use-package marginalia
  :ensure t
  :bind (("M-A" . marginalia-cycle)
         :map minibuffer-local-map
         ("M-A" . marginalia-cycle))
  :init
  (marginalia-mode)
  :config
  (setq marginalia-field-width 50)
  )

(use-package rainbow-delimiters
  :ensure t
  :hook
  (c++-mode        . rainbow-delimiters-mode)
  (python-mode     . rainbow-delimiters-mode)
  (emacs-lisp-mode . rainbow-delimiters-mode)
  (c++-ts-mode        . rainbow-delimiters-mode)
  (python-ts-mode     . rainbow-delimiters-mode)
  )

(use-package dired
  :config
  (put 'dired-find-alternate-file 'disabled nil)
  :bind (
         :map dired-mode-map
              ("C-c h" . dired)
              ("C-c o" . 'open-file-external)))

(use-package dired-x
  :config
  (setq dired-omit-files "^\\...+$")
  )

(use-package which-key
  :ensure t
  :delight
  :config
  (which-key-mode))

(use-package magit
  :ensure t
  :bind ("C-x g" . magit-status))

(use-package flymake
  :bind(("M-n" . flymake-goto-next-error)
        ("M-p" . flymake-goto-prev-error)))

(use-package compile
  :init
  (require 'ansi-color)
  (defun colorize-compilation-buffer ()
    (ansi-color-apply-on-region compilation-filter-start (point))
    )
  :hook
  (compilation-filter . colorize-compilation-buffer)
  )

(defun pulse-line (&rest _)
  "Pulse the current line."
  (pulse-momentary-highlight-one-line (point)))

(dolist (command '(scroll-up-command scroll-down-command
                                     recenter-top-bottom other-window))
  (advice-add command :after #'pulse-line))

(defun open-init-file ()
  "Edit the `user-init-file` in this window."
  (interactive)
  (find-file user-init-file))

(defun raise-shell()
  "Raise or create a new shell."
  (interactive)
  (if (get-buffer-window "*shell*" 'visible)
      (select-window (get-buffer-window "*shell*"))
    (eshell)))

(defun open-file-external (file)
  (interactive "f")
  (let ((process-connection-type nil))
    (start-process
     "" nil shell-file-name
     shell-command-switch
     (format "nohup 1>/dev/null 2>/dev/null xdg-open %s"
             (expand-file-name file)))))

(defun mp-split-below (arg)
  "Split window below from the parent or from root with ARG."
  (interactive "P")
  (split-window (if arg (frame-root-window)
                  (window-parent (selected-window)))
                nil 'below nil))

(defun mp-split-right (arg)
  "Split window right from the parent or from root with ARG."
  (interactive "P")
  (split-window (if arg (frame-root-window)
                  (window-parent (selected-window)))
                nil 'right nil))

(provide 'init)
